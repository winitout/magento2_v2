<?php

namespace Cibler\Shop\Model;

use Cibler\Shop\Api\DeleteCartPriceRuleInterface;
use Magento\SalesRule\Api\RuleRepositoryInterface;

class DeleteCartPriceRule implements DeleteCartPriceRuleInterface {

    protected $resourceConnection;

    protected $ruleRepository;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\SalesRule\Model\Coupon $coupon,
        \Magento\SalesRule\Model\Rule $saleRule,
        \Magento\Store\Model\Website $websiteModel,
        RuleRepositoryInterface $ruleRepository,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->coupon = $coupon;
        $this->saleRule = $saleRule;
        $this->_websiteModel = $websiteModel;
        $this->ruleRepository = $ruleRepository;
        $this->request = $request;
    }

    public function deleteRule($apikey, $coupons) {
        try {
            $body = $this->request->getContent();
            $bodyArray = json_decode($body, true);
            
            $coupons = $bodyArray['coupons'];
            $apikey = $bodyArray['apikey'];
            $valid = 0;
            $notValid = 0;

            if(!empty($coupons) && $apikey) {
                $tableName = $this->getTableName('core_config_data');
                $checkKeyExist = 'SELECT * FROM ' . $tableName . ' WHERE path="cshop/general/keyapi" && scope="websites" && value="' . $apikey .'"';
                $keyResult = $this->resourceConnection->getConnection()->fetchAll($checkKeyExist);

                if (!count($keyResult)) {
                    header("HTTP/1.1 401 Unauthorized");
                    echo "Invalid api key";
                    exit;
                }

                foreach ($coupons as $coupon) {
                    foreach($coupon as $value) {
                        $ruleId =  $this->coupon->loadByCode($value)->getRuleId();

                        if ($ruleId) {
                            $rule = $this->saleRule->load($ruleId);
                            $websiteIds = $rule->getWebsiteIds();

                            if (!empty($websiteIds)) {
                                $tableName = $this->getTableName('core_config_data');
                                $query = 'SELECT * FROM ' . $tableName . ' WHERE path="cshop/general/keyapi" && scope="websites" && value="' . $apikey .'" && scope_id IN (' . implode(',',$websiteIds) . ')';
                                $results = $this->resourceConnection->getConnection()->fetchAll($query);

                                if (count($results)) {
                                    try {
                                        $salesRule = $this->ruleRepository->deleteById($ruleId);

                                        $valid = $valid + 1;
                                    } catch (Exception $exception) {
                                        echo $exception->getMessage();
                                        $notValid = $notValid + 1; 
                                    }
                                } else {
                                    $notValid = $notValid + 1;
                                }
                            } else {
                                $notValid = $notValid + 1;                                
                            }
                        } else {
                            $notValid = $notValid + 1;
                        }

                    }
                }
            } else {
                $notValid = $notValid + 1;               
            }

            if (!$valid && $notValid) {
                $response=['error' => "Something went wrong while deleting the coupon(s)"];
            } else if ($valid && !$notValid) {
                $response=['success' => "All coupon(s) are deleted successfully"];
            } else if ($valid && $notValid) {
                $response=['success' => "Some coupon(s) deleted successfully and something went wrong with some coupon(s)"];
            }
        } catch (\Exception $e) {
            $response=['error' => $e->getMessage()];
        }

        return json_encode($response);
    }

    public function getTablename($tableName)
    {
        $connection  = $this->resourceConnection->getConnection();
        $tableName   = $connection->getTableName($tableName);
        return $tableName;
    }

}