<?php

namespace Cibler\Shop\Observer;

use Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory as RuleCollection;

class CheckCouponCibler implements \Magento\Framework\Event\ObserverInterface {

	protected $_ruleCollection;
	protected $_customerSession;
	protected $_storeManager;
	protected $logger;
	protected $scopeConfig;
	protected $ruleFactory;
    protected $couponModel;
    protected $productRuleFactory;

	public function __construct(
		RuleCollection $ruleCollection,
		\Magento\Customer\Model\SessionFactory $customerSession,
		\Magento\Checkout\Model\Cart $cart,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Psr\Log\LoggerInterface $logger,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\SalesRule\Model\RuleFactory $ruleFactory,
        \Magento\SalesRule\Model\Coupon $couponModel,
        \Magento\SalesRule\Model\Rule\Condition\ProductFactory $productRuleFactory
	) {
	    $this->_ruleCollection = $ruleCollection;
	    $this->_customerSession = $customerSession->create();
	    $this->_cart = $cart;
	    $this->_storeManager = $storeManager;
	    $this->logger = $logger;
	    $this->scopeConfig = $scopeConfig;
	    $this->ruleFactory = $ruleFactory;
        $this->couponModel = $couponModel;
        $this->productRuleFactory = $productRuleFactory;
	}

	public function CheckCustomerIsLoggedIn() {
        return $this->_customerSession->isLoggedIn();
    }

    public function getCartData() {
        return $this->_cart->getQuote();
    }

    public function getCurrentCurrencyCode() {
        return $this->_storeManager->getStore()->getCurrentCurrencyCode();
    }

    public function getWebsiteId() {
        return $this->_storeManager->getStore()->getWebsiteId();
    }

    public function getCustomerData() {
        if ($this->_customerSession->isLoggedIn()) {
            return $this->_customerSession->getCustomerData();
        }
        return false;
    }

    public function getKeyapi() {
    	$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
    	return $this->scopeConfig->getValue('cshop/general/keyapi', $storeScope);
    }

    public function getCustomerId() {
    	$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
    	return $this->scopeConfig->getValue('cshop/general/customerid', $storeScope);
    }

    public function getEnvUrl() {
    	$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
    	return $this->scopeConfig->getValue('cshop/general/envurl', $storeScope);
    }

	public function execute(\Magento\Framework\Event\Observer $observer) {
		$event = $observer->getEvent();
        $event_name = $event->getName();

        if($event_name == 'controller_action_predispatch_checkout_cart_couponpost') {
        	$controller = $observer->getControllerAction();
        	$code = $controller->getRequest()->getParam('coupon_code');
        } else {
        	$code = $observer->getData('coupon_code');
        }

		$result = $this->CheckCoupon($code);
		
        if ($result) {
        	$jsn = json_decode($result);
			
        	if($jsn->canUse) {
        		$cart = $this->getCartData();
		        $rules_object = $this->ruleFactory->create();
		        $rules = $rules_object->getCollection();
		        $coupon_collection = array();

		        foreach ($rules as $coupon) {
                    if($coupon->getUseAutoGeneration()) {
                        $resultAutoGenerate = $this->couponModel->load($coupon->getCode(), 'code');
                        if($resultAutoGenerate) {
                            $coupon_collection[] = $coupon->getCode();
                        }
                    } else {
                        $coupon_collection[] = $coupon->getCode();
                    }
		        }
		        
		        $nb_coupon = $cart->getCouponCode();
		        if($jsn->combinable) {
		        	if (!in_array($code, $coupon_collection)) {
		        		$this->AddCoupon($code, $result);
		        	}
		        } else {
		        	if ($nb_coupon == 0) {
		        		if (!in_array($code, $coupon_collection)) {
		        			$this->AddCoupon($code, $result);
		        		}
		        	} else {
		        		$this->logger->debug("Cibler submitAddDiscount false.");
		        	}
		        }
        	}
        } else {
			$this->logger->debug("Cabler il y a aucun retour sur la requête API , Svp verifier votre Acces API");
        }

        return $this;
	}

	public function AddCoupon($code,$json) {
		$result = json_decode($json);

        $ruleModel = $this->ruleFactory->create();

        $ruleModel->setName($result->description)
            ->setDescription($result->description)
            ->setFromDate(date('Y-m-d H:i:s'))
            ->setToDate(date('Y-m-d H:i:s', strtotime($result->expirationDate)))
            ->setUsesPerCustomer(1)
            ->setCustomerGroupIds(array('0','1','2','3'))
            ->setIsActive('1')
            ->setApplyToShipping(0)
            ->setTimesUsed(1)
            ->setWebsiteIds(array($this->getWebsiteId(),))
            ->setCouponType('2')
            ->setCouponCode($code)
            ->setUsesPerCoupon(NULL);

        if ($result->discountType == "PERCENT") {
           
            $ruleModel->setSimpleAction('by_percent')
            ->setDiscountAmount($result->value);
        }
        if ($result->discountType == "VALUE") { 
            $ruleModel->setSimpleAction('by_fixed') 
            ->setDiscountAmount($result->value);
        }

        /* SKU Condition Start Here */

        if($result->products) {
            $skus = array();

            foreach($result->products as $product) {
                $skus[] = $product;
            }

            $skuCond = $this->productRuleFactory->create()
                ->setType('Magento\SalesRule\Model\Rule\Condition\Product')
                ->setData('attribute','sku')
                ->setData('operator','()')
                ->setValue(implode(',',$skus));
            $ruleModel->getActions()->addCondition($skuCond);
        }

        /* SKU Condition End Here */

        /* cat condition start here */

        if($result->categories) {
            $catgs = array();

            foreach($result->categories as $product) {
                $catgs[] = $product;
            }

            $catsCond = $this->productRuleFactory->create()
                ->setType('Magento\SalesRule\Model\Rule\Condition\Product')
                ->setData('attribute','category_ids')
                ->setData('operator','()')
                ->setValue(implode(',',$catgs));

            $ruleModel->getActions()->addCondition($catsCond);

        }

        /* cat condition end here */

        $ruleModel->save();

        $this->logger->debug("Cabler: coupon created " . $code);

	}

	public function CheckCoupon($code) {
		$customer = $this->CheckCustomerIsLoggedIn();
		/*if (!$customer) {
			return;
		}*/

		$cartItems = $this->getCartData();

		$items = array();
		$grandTotal = $cartItems->getGrandTotal();

		foreach ($cartItems->getAllItems() as $item) {
			$categoriesIds = $item->getProduct()->getCategoryIds();
			
			if (count($categoriesIds)) {
                $categoryId = $categoriesIds[0];
            } else {
				$categoryId = 0;
            }
			
			$items[] = array(
                "productId" => $item->getSku(),
                "quantity" => $item->getQty(),
                "name" => $item->getProduct()->getName(),
                "unitPrice" => $item->getProduct()->getPrice(),
                "priceCurrencyCode" => $this->getCurrentCurrencyCode(),
                "category" => 4
				//"category" => $categoryId
            );
		}

		if(count($items) > 0) {
			$total = $cartItems->getGrandTotal();
		}

		$customerData = $this->getCustomerData();
		
		$custEmail = "";

        if ($customer) {
            $custEmail = $customerData->getEmail();
        }

		$post = array(
            "email" => $custEmail,
            "code" => $code,
            "cart" => array(
                "total" => $total,
                "items"=>$items
            )
    	);

    	$this->logger->debug("return coupon: " . print_r($post, true));

    	$env_url = $this->getEnvUrl();
        $customer_id = $this->getCustomerId();

        $path = "/api/giftCodes/validate/".$customer_id;
        $data_string = json_encode($post);
        $ch = curl_init($env_url.$path);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        curl_close($ch);
        $this->logger->debug("return coupon: " . print_r($result, true));
        return $result;
	}

}