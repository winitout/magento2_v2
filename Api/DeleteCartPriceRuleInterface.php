<?php

namespace Cibler\Shop\Api;

interface DeleteCartPriceRuleInterface {
    /**
     * GET for Post api
     * @param string $apikey
     * @param mixed $coupons
     * @return string
     */
    public function deleteRule($apikey,$coupons);
}
